﻿using JusAdBellum;
using JusAdBellumUnits.CultureDescriptor;
using TaleWorlds.MountAndBlade;

namespace JusAdBellumUnits
{
    public class SubModule : MBSubModuleBase
    {
        protected override void OnSubModuleLoad()
        {
            Jab.descriptorList.Add(new AseraiDescriptor());
            Jab.descriptorList.Add(new BattanianDescriptor());
            Jab.descriptorList.Add(new ImperialDescriptor());
            Jab.descriptorList.Add(new KhuzaitDescriptor());
            Jab.descriptorList.Add(new SturgianDescriptor());
            Jab.descriptorList.Add(new VlandianDescriptor());
        }
    }
}