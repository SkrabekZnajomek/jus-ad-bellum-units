﻿using System.Collections.Generic;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;

namespace JusAdBellumUnits.CultureDescriptor
{
    class BattanianDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        public BattanianDescriptor()
        {
            CultureName = "Battania";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_battanian_volunteer");
            AddUnitToPersistentDictionary(UnitType.Levy, "jab_battanian_clanwarrior");
            AddUnitToPersistentDictionary(UnitType.Regular, "jab_battanian_picked_warrior");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_battanian_oathsworn");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_battanian_sellsword");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_battanian_highborn_youth");
        }
        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 3:
                    return new float[3] { 0.0f, 0.4f, 0.6f };
                case 4:
                    return new float[3] { 0.0f, 0.3f, 0.7f };
                case 5:
                case 6:
                    return new float[4] { 0.0f, 0.3f, 0.5f, 0.2f };
            }

            return new float[6] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }
    }
}
