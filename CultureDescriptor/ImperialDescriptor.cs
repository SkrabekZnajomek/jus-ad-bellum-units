﻿using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.ObjectSystem;
using JusAdBellum.Extensions;
using JusAdBellum.CultureUnitLists;

namespace JusAdBellumUnits.CultureDescriptor
{
    class ImperialDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        bool Tutorial = false;
        public ImperialDescriptor()
        {
            CultureName = "Empire";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_imperial_volunteer");
            AddUnitToPersistentDictionary(UnitType.Levy, "jab_imperial_conscript");
            AddUnitToPersistentDictionary(UnitType.Regular, "jab_imperial_legionary");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_imperial_praetorian");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_imperial_mercenary");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_imperial_vigla_recruit");
            AddUnitToPersistentDictionary(UnitType.Additional4, "tutorial_placeholder_volunteer");
        }

        public override bool CanPlayerRecruitMercenary()
        {
            if (Tutorial == true)
            {
                return true;
            }
            return base.CanPlayerRecruitMercenary();
        }

        public override List<UnitType> GetTroopListForPlayer()
        {
            if (Tutorial == true)
            {
                return new List<UnitType>
                {
                    UnitType.Additional4,
                    UnitType.Invalid,
                    UnitType.Invalid,
                    UnitType.Invalid,
                    UnitType.Invalid,
                };
            }
            return base.GetTroopListForPlayer();
        }

        public override void PrepareTutorialUnits()
        {
            Tutorial = true;
            JL.Log(JLT.TutorialPatches, $"PrepareTutorialUnits");
        }

        public override void ClearTutorialUnits()
        {
            Tutorial = false;
        }

        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[3] { 0.0f, 0.5f, 0.5f };
                case 4:
                    return new float[3] { 0.0f, 0.3f, 0.7f };
                case 5:
                case 6:
                case 7:
                    return new float[4] { 0.0f, 0.3f, 0.4f, 0.3f };
            }

            return new float[6] { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }

    }
}
