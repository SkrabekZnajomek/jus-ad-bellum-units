﻿using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;

namespace JusAdBellumUnits.CultureDescriptor
{
    class VlandianDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        public VlandianDescriptor()
        {
            CultureName = "Vlandia";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_vlandian_levy");
            AddUnitToPersistentDictionary(UnitType.Regular, "jab_vlandian_soldier");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_vlandian_yeoman");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_vlandian_mercenary");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_vlandian_squire");
        }

        public override CharacterObject GetRecruitForTown()
        {
            float unitPicker = MBRandom.RandomFloat;

            if (unitPicker < 0.4)
                return Units[UnitType.Volunteer];
            else
                return Units[UnitType.Regular];
        }

        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[3] { 0.3f, 0.0f, 0.7f };
                case 4:
                    return new float[3] { 0.3f, 0.0f, 0.7f };
                case 5:
                case 6:
                case 7:
                    return new float[4] { 0.2f, 0.0f, 0.5f, 0.3f };
            }

            return new float[6] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }
    }
}
