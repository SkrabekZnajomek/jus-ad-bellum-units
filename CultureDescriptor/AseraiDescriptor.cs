﻿using System.Collections.Generic;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;

namespace JusAdBellumUnits.CultureDescriptor
{
    class AseraiDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        public AseraiDescriptor()
        {
            CultureName = "Aserai";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_aserai_tribesman");
            AddUnitToPersistentDictionary(UnitType.Levy, "jab_aserai_levy");
            AddUnitToPersistentDictionary(UnitType.Regular, "jab_aserai_mameluke_soldier");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_aserai_elite_infantry");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_aserai_mercenary");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_aserai_youth");
        }

        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[3] { 0.0f, 0.4f, 0.6f };
                case 4:
                    return new float[3] { 0.0f, 0.6f, 0.4f };
                case 5:
                case 6:
                case 7:
                    return new float[4] { 0.0f, 0.1f, 0.5f, 0.4f };
            }

            return new float[6] { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }
    }
}
