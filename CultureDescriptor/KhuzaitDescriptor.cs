﻿using System.Collections.Generic;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;

namespace JusAdBellumUnits.CultureDescriptor
{
    class KhuzaitDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        public KhuzaitDescriptor()
        {
            CultureName = "Khuzait";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_khuzait_nomad");
            AddUnitToPersistentDictionary(UnitType.Levy, "jab_khuzait_hunter");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_khuzait_choosen_archer");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_khuzait_mercenary_nomad");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_khuzait_noble_son");
        }

        public override CharacterObject GetRecruitForTown()
        {
            float unitPicker = MBRandom.RandomFloat;

            if (unitPicker < 0.35)
                return Units[UnitType.Volunteer];
            else
                return Units[UnitType.Levy];
        }

        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[2] { 0.5f, 0.5f };
                case 4:
                    return new float[2] { 0.45f, 0.55f };
                case 5:
                case 6:
                case 7:
                    return new float[4] { 0.35f, 0.45f, 0.0f, 0.2f };
            }

            return new float[6] { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }
    }
}
