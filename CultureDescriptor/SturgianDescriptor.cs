﻿using System.Collections.Generic;
using JusAdBellum.CultureUnitLists;
using TaleWorlds.CampaignSystem;

namespace JusAdBellumUnits.CultureDescriptor
{
    class SturgianDescriptor : JusAdBellum.CultureUnitLists.CultureDescriptor
    {
        public SturgianDescriptor()
        {
            CultureName = "Sturgia";
            AddUnitToPersistentDictionary(UnitType.Volunteer, "jab_sturgian_volunteer");
            AddUnitToPersistentDictionary(UnitType.Levy, "jab_sturgian_recruit");
            AddUnitToPersistentDictionary(UnitType.Regular, "jab_sturgian_spearman");
            AddUnitToPersistentDictionary(UnitType.Elite, "jab_nord_warrior");
            AddUnitToPersistentDictionary(UnitType.Mercenary, "jab_nord_mercenary");
            AddUnitToPersistentDictionary(UnitType.NobleUnit, "jab_varyag");
        }

        public override float[] GetDispositionForClan(int clanTier, CultureObject culture)
        {
            if (culture != Culture)
                return new float[5] { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
            switch (clanTier)
            {
                case 2:
                case 3:
                    return new float[3] { 0.0f, 0.5f, 0.5f };
                case 4:
                    return new float[3] { 0.0f, 0.6f, 0.4f };
                case 5:
                case 6:
                case 7:
                    return new float[4] { 0.0f, 0.5f, 0.3f, 0.2f };
            }
            return new float[6] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
        }

    }
}
